package main

import (
	"flag"
	"html/template"
	"io"
	"log"
	"os"
	"net"
	"net/http"
	"strings"

	"github.com/gorilla/websocket"
)

var addr = flag.String("addr", "localhost:7000", "http service address")
var noCert = flag.Bool("nossl", false, "use localhost.pem certificat")

var upgrader = websocket.Upgrader{CheckOrigin: checkOrigin} // use default options

func checkOrigin(r *http.Request) bool {
	return true
}

// StartServer starts TCP listener
func StartServer(port string, ws *websocket.Conn) {
	ln, err := net.Listen("tcp", port)
	if err != nil {
		log.Println("error tcp connection:", port, err)
		ws.Close()
		return
	}

	log.Println("Listening on", port)
	for {
		tcp, err := ln.Accept()
		if err != nil {
			log.Println("error tcp connection:", port, err)
			ln.Close()
			ws.Close()
			return
		}

		log.Printf("[%s]: Connection has been opened\n", tcp.RemoteAddr().String())
		go ws2tcp(ws, tcp, nil, ln)
		go tcp2ws(tcp, ws, ln)
	}
}

// StartClient starts TCP connector
func StartClient(host string, ws *websocket.Conn) {
	openTCP := func() net.Conn {
		tcp, err := net.Dial("tcp", host)
		if err != nil {
			log.Println("error tcp connection:", host, err)
			return nil
		}
		log.Println("Connected to", host)
		log.Printf("Forward all connexion ws <-> %s,%s ", tcp.LocalAddr().String(), tcp.RemoteAddr().String())
		go tcp2ws(tcp, ws, nil)

		return tcp
	}

	go ws2tcp(ws, nil, openTCP, nil)
}

func ws2tcp(ws *websocket.Conn, tcp net.Conn, openTCP func() net.Conn, server net.Listener) {
	log.Println("conect ws -> tcp")
	for {
		messageType, r, err := ws.NextReader()
		if err != nil {
			log.Println("error reader ws2tcp", tcp.LocalAddr(), err)
			//probleme ws, on close tout
			ws.Close()
			tcp.Close()
			if server != nil {
				server.Close()
			}
			return
		}

		if messageType == websocket.BinaryMessage {
			if tcp == nil && openTCP != nil {
				tcp = openTCP()
			}

			if _, err := io.Copy(tcp, r); err != nil {
				log.Println("error writer ws2tcp", tcp.LocalAddr(), err)
				// on ne peut pas ecrire sur le tcp, si on est en mode client, on ferme tout
				// sinon (mode server) on ferme juste la connexion tcp actuelle
				if err != io.EOF {
					tcp.Close()
					ws.Close()
					if server != nil {
						server.Close()
					}
				}
				return
			}
		}
	}
}

func tcp2ws(tcp net.Conn, ws *websocket.Conn, server net.Listener) {
	log.Println("conect tcp -> ws")
	data := make([]byte, 1024)
	for {
		// log.Printf("Try to read tcp (%v -> %v)\n", tcp.LocalAddr().String(), tcp.RemoteAddr().String())
		n, err := tcp.Read(data)
		if err != nil {
			log.Println("Error tcp2ws, close all", err, tcp.LocalAddr().String(), tcp.RemoteAddr().String())
			// on ne peut pas lire sur le tcp, si on est en mode client, on ferme tout
			// sinon (mode server) on ferme juste la connexion tcp actuelle
			if err != io.EOF {
				tcp.Close()
				ws.Close()
				if server != nil {
					server.Close()
				}
			}
			return
		}

		if n > 0 {
			// log.Println("Receive tcp data", n)
			ws.WriteMessage(websocket.BinaryMessage, data[:n])
		}
	}
}

// func tcp2ws(tcp net.Conn, ws *websocket.Conn) {
// 	w, err := ws.NextWriter(2) //  TODO type
// 	defer func() {
// 		tcp.Close()
// 		ws.Close()
// 	}()

// 	_, err = io.Copy(w, tcp)
// 	if err != nil {
// 		log.Printf("[%s]: ERROR: %s\n", tcp.RemoteAddr(), err)
// 	}

// }

func connect(w http.ResponseWriter, r *http.Request) {
	log.Println("Try to upgrade to ws")
	cors(w, r)
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	log.Println("AAA")

	var host string
	// wait for command
	for {
		log.Println("BBB")
		messageType, message, err := ws.ReadMessage()
		log.Println("CCC")
		if err != nil {
			log.Println("read ws:", err)
			return
		}

		if messageType == websocket.TextMessage {
			host = string(message)
			break
		} else {
			log.Println("received not command message", message)
		}
	}
	log.Println("DDD")

	if strings.HasPrefix(host, ":") {
		// list,
		log.Println("try to listen on local port", host)
		go StartServer(host, ws)
	} else {
		// send (client)
		log.Println("try to connect to host", host)
		go StartClient(host, ws)
	}
}

func home(w http.ResponseWriter, r *http.Request) {
	if strings.HasPrefix(r.Header.Get("Origin"), "http:") {
		log.Println("return home for ws")
		homeTemplate.Execute(w, "ws://"+r.Host+"/connect")
	} else {
		log.Println("return home for wss")
		homeTemplate.Execute(w, "wss://"+r.Host+"/connect")
	}
}

func downloadWindows(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "webcat.exe")
}

func downloadLinux(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "webcat")
}

func cors(w http.ResponseWriter, r *http.Request) {
	log.Println("OOOO", r)
	w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	if r.Method == http.MethodOptions {
		log.Println("OPTION request")
		w.Header().Set("Access-Control-Allow-Methods", r.Header.Get("Access-Control-Request-Method"))
		if r.Header.Get("Access-Control-Request-Headers") != "" {
			w.Header().Set("Access-Control-Allow-Headers", strings.Join(r.Header.Values("Access-Control-Request-Headers"), ","))
		}
		return
	}
}

func writeFile(filename string, content string) error {
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	l, err := f.WriteString(content)
	if err != nil {
		f.Close()
		return err
	}
	log.Println(l, "bytes written successfully in file ", filename)
	err = f.Close()
	if err != nil {
		return err
	}

	return nil
}

func main() {
	flag.Parse()
	log.SetFlags(0)
	http.HandleFunc("/connect", connect)
	http.HandleFunc("/", home)
	http.HandleFunc("/webcat", downloadLinux)
	http.HandleFunc("/webcat.exe", downloadWindows)

	if *noCert {
		log.Println("listen on", *addr, "nossl")
		log.Fatal(http.ListenAndServe(*addr, nil))
	} else {
		log.Println("listen on", *addr, "ssl")
		err := writeFile("localhost.pem", cert)
		if err != nil {
			log.Fatal("can't where cert", err)
		}
		err = writeFile("localhost-key.pem", certKey)
		if err != nil {
			log.Fatal("can't where cert key", err)
		}
		log.Fatal(http.ListenAndServeTLS(*addr, "localhost.pem", "localhost-key.pem", nil))
	}
}

var cert = `-----BEGIN CERTIFICATE-----
MIIEDzCCAnegAwIBAgIRAM7TztPzLIYoaBYjGyFIfi8wDQYJKoZIhvcNAQELBQAw
VzEeMBwGA1UEChMVbWtjZXJ0IGRldmVsb3BtZW50IENBMRYwFAYDVQQLDA1wb3Vz
c2luQHBvdWFjMR0wGwYDVQQDDBRta2NlcnQgcG91c3NpbkBwb3VhYzAeFw0xOTA2
MDEwMDAwMDBaFw0zMDA0MTMwODUwMzJaMEExJzAlBgNVBAoTHm1rY2VydCBkZXZl
bG9wbWVudCBjZXJ0aWZpY2F0ZTEWMBQGA1UECwwNcG91c3NpbkBwb3VhYzCCASIw
DQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALNv+a0ZQ4Qkm+S9ie6QEH6BZ3a0
OdhlfkKak5vRAhyWo4iAQ9Ar3D5+QNHntv4cILdA/g8YLIOaJoVXgC902fmbAAUu
kWb7dJDAeAKwd463ouGyUvLfF4e9yqfA019+zsTVkQmdEykx5PP7DMgYdA6uMQWt
KEJiiJxmKe04bdwNjvVhQrZadd0RvCIT6Oi2kQesCuWeVAQ6W3OoYCcK976FOprU
C0pi/WGPbFxZR75OznFAflT/C0pY7NTJQpGagREHR0KGJU7IAyCNM02RFvxWKkHL
mAkPApx4WHygd/wSbr2Ndv0XB6Rf6gSZDE6Xi78BN4mSIGhCCth4PgSgs50CAwEA
AaNsMGowDgYDVR0PAQH/BAQDAgWgMBMGA1UdJQQMMAoGCCsGAQUFBwMBMAwGA1Ud
EwEB/wQCMAAwHwYDVR0jBBgwFoAUJIDnDxxo6tGP6XbLUPowl1RyocQwFAYDVR0R
BA0wC4IJbG9jYWxob3N0MA0GCSqGSIb3DQEBCwUAA4IBgQB88XPknvHYEYxU/mp5
M0XxWKUjsjhvjZ+k8wf1Ao8nnPzPzqj+HkNSeg9GdTti3ERv9VDln8IMFv4ALVQ3
eps6j2UbRBEn9F6AcNx3OfLDzbA7RC1/nX1NwCrlkeNT0cOG5wgVtMq7qAz2fiyP
ehVLv7531pwjyZQUmZ07TOt5BcvTVHsQpi1O908ShCS/K72juIY7rnyomAStqZGh
i5R69+fC0QEPDWQ9ATZ7A2vXWa8WdL4nCetAOMZI3U/SRvYWprICMitNhlsZKYBf
8LjH2cenZdeZmjnu1fuyyrqbn3I8EpbPt3w9RXFaH0ONr1L39w6xkxtctIaCqXkX
RsXspdzwPzMIhxmXjTKpJ9v0eNrxJDrEk0pIubwVi/sZLX7XKRvRHCWKA1HxGn5h
zKQV+EzT+4iM4jL1yRsgGQvN1r1lFJ9i/qMT5g7ggpHS+jO6rO2kA4tNDHuhZENV
T0fZx5Hrg8MhO4wV6O5O8Zfuw92aOnRzHlXzi+UhZyvzxO4=
-----END CERTIFICATE-----`

var certKey = `-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCzb/mtGUOEJJvk
vYnukBB+gWd2tDnYZX5CmpOb0QIclqOIgEPQK9w+fkDR57b+HCC3QP4PGCyDmiaF
V4AvdNn5mwAFLpFm+3SQwHgCsHeOt6LhslLy3xeHvcqnwNNffs7E1ZEJnRMpMeTz
+wzIGHQOrjEFrShCYoicZintOG3cDY71YUK2WnXdEbwiE+jotpEHrArlnlQEOltz
qGAnCve+hTqa1AtKYv1hj2xcWUe+Ts5xQH5U/wtKWOzUyUKRmoERB0dChiVOyAMg
jTNNkRb8VipBy5gJDwKceFh8oHf8Em69jXb9FwekX+oEmQxOl4u/ATeJkiBoQgrY
eD4EoLOdAgMBAAECggEAHc7hNC85Nw950kLmUfoes1U94ctIvLkHg3r4QG81IZio
q/layo8utDb2/6e1j3sOuEKUsd87LdWCxq3lfnyz5TIpi7LTtr7Y+17t0MyN3+iD
p2ifPPdwij0uGDYBJyECqlIsGOO+Fh7SwPEYi3j8dp/bRu0f/f8uYITcPCsenOpg
UCCiB8pMhLru6AuBPgsSNSTGMf03N5sL7K+tD3m89bqnA0anFiJPY+tFP99a2Kxa
spNYynUH4gigjgYIzXS25rZEXDhuZEYDX7xjlUoDmxCwb13c2INWqb6awUYeqKhd
AlytvjllXrpv4+6QxvGdp9mKO/RjKl6WaLuuCeRNQQKBgQDlfyOGXC5Tm+n7mVQg
rMsIFYjcDIqQuK5nN1QviOMXR6YDin7Zf7mZfiejD/H3cp2Mfe1utCUMQpQxd/+y
ui5M/mfP1HiQAdCx/VSYteNJzgnjjcxFu+eReROWVhTHId1swrjMNqOKCRCsHObR
MNVmPcMUMfGkx2y3sT8CZdMVzQKBgQDIKOFkNfPhYL9zhoFwZTQqJ5v9zX+eN5+3
kzk5mMCRfg2LVzZz6zg6JSLEl5yOfYOzpMkkIywBi3vG2HttkdTteQwTydWOy0x+
dJwP0ZqjyzmwKdIHKhcGW6IqfH5KDZvX8Vaaw6ZQhuFSTol2yJSV0lbBaszH8BUN
wUl8k6xFEQKBgQCd6cDhB/bckxEKjbOklCFUW7UDbSG/eUSQQzMLTk6sSyHBl/+2
2zszkenmOueNE/GypUfF//DqIbnk3CQ2sAOB+SAnkpSuyoD2qFEYjzdpJnwJdx2S
8jviDoTa0jqT1wqMduQ/qG0ZYyl0tKJiWQ7NvjvIXqFed9aoAy9PEA+SFQKBgGZQ
X0Y2Pf/K0JJhAvFNCikyLg7CaKWbkUzpOKs0Zdr5MDs1zWPbrNYr/WW4ihJcHmJJ
rXeHcwsnHZI7Pnx3uvvYkE/dMAjZ6rQJL2z2m7F2RHrEJdP7s+FQHcjR8kwwQKVV
fAuITw+CotLLyoCpbypWZnpf+WWKV3i8gK/pXHnBAoGBAM/kW0dILDkY+r+m29kl
BuFA46umx4uyR1/beiXI3loPwygiK5PQD4cXxcxYRkOhuD1+Wg43HBBIQf1rzhvk
U1+uFSP1d2vTxm1m5117Mqj9OkoztmqOXKzieJyRpadMAoZ9Tt3ZcPBLRx4jF92J
6hMacn0NKjnTZUjaLUtif+gm
-----END PRIVATE KEY-----`

var homeTemplate = template.Must(template.New("").Parse(`
							<!DOCTYPE html>
							<html>
							<head>
							<meta charset="utf-8">
							<script>  
							window.addEventListener("load", function(evt) {
								let output = document.getElementById("output");
								let server = document.getElementById("server");
								let proxy = document.getElementById("proxy");
								let destination = document.getElementById("destination");
								
								let opened = 0;
								let wserver;
								let wproxy;
								
								let print = function(message) {
									var d = document.createElement("div");
									d.textContent = message;
									output.appendChild(d);
									};
									
									let openws = function(wshost, command) {
										let ws = new WebSocket(wshost);
										
										ws.onopen = function(evt) {
											print(wshost + "(" + command + ") SERVER OPENED " + ws.readyState);
											opened++
											ws.send(command)
										}
										ws.onclose = function(evt) {
											print(wshost + "(" + command + ") SERVER CLOSED " + ws.readyState);
											opened--
											if (ws === wserver) {
												wproxy.close()
												} else {
													wserver.close()
												}
												} 
												
												ws.onerror = function(evt) {
													print(wshost + " ERROR: " + evt.data);
												}
												
												return ws;
											}
											
											document.getElementById("open").onclick = function(evt) {
												if (opened === 2) {
													return false;
												}
												wserver = openws("{{.}}", server.value);
												wproxy = openws(proxy.value + "/connect", destination.value);
												
												// bridge between websocket
												wserver.onmessage = function(evt) {
													console.log("server send", evt.data)
													wproxy.send(evt.data);
												}
												
												wproxy.onmessage = function(evt) {
													console.log("proxy send", evt.data)
													wserver.send(evt.data);
												}
												
												return false;
												};
												
												document.getElementById("close").onclick = function(evt) {
													wserver.close();
													wproxy.close();
													return false;
													};
													});
													</script>
													</head>
													<body>
													download <a href="webcat">linux (amd64)</a> <a href="webcat.exe">windows (386)</a>
													<table>
													<tr><td valign="top" width="50%">
													<p>Click "Open" to create a connection to the server, 
													"Send" to send a message to the server and "Close" to close the connection. 
													You can change the message and send multiple times.
													<p>
													server port<input type="text" id="server" value=":7777"> (:0 for random assignment, your client must can connect to this) <br/>
													proxy host <input type="text" id="proxy" value="wss://localhost:7000"> (other webcat instance) <br/>
													destination host<input type="text" id="destination" value="pouzo:22"> (service to connect) <br/>
													<button id="open">Open</button>
													<button id="close">Close</button>
													</td><td valign="top" width="50%">
													<div id="output"></div>
													</td></tr></table>
													</body>
													</html>
													`))
