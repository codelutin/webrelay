all: webrelay.exe webrelay

webrelay: webrelay.go
	GOPROXY=https://proxy.golang.org CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -ldflags="-s -w" webrelay.go

webrelay.exe: webrelay.go
	GOPROXY=https://proxy.golang.org CGO_ENABLED=0 GOOS=windows GOARCH=386 go build -a -ldflags="-s -w" webrelay.go

clean:
	rm webrelay webrelay.exe
